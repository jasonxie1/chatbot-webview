// https://github.com/zhiyuan-lin/liff.d.ts
import { Message } from '@line/bot-sdk';

declare var liff: LIFF;

type LiffErrorCode = 'INIT_FAILED' | 'INVALID_ARGUMENT' | 'UNAUTHORIZED'
    | 'FORBIDDEN' | 'INVALID_CONFIG' | 'INVALID_ID_TOKEN'
    | 'INVALID_ARGUMENT' | 'THINGS_NO_LINKED_DEVICES' | 'BLUETOOTH_SETTING_OFF'
    | 'THINGS_TERMS_NOT_AGREED' | 'BLUETOOTH_NO_LOCATION_PERMISSION'
    | 'BLUETOOTH_LOCATION_DISABLED' | 'BLUETOOTH_LE_API_UNAVAILABLE'
    | 'BLUETOOTH_CONNECT_FAILED' | 'BLUETOOTH_ALREADY_CONNECTED'
    | 'BLUETOOTH_CONNECTION_LOST' | 'BLUETOOTH_UNSUPPORTED_OPERATION'
    | 'BLUETOOTH_SERVICE_NOT_FOUND' | 'BLUETOOTH_CHARACTERISTIC_NOT_FOUND';

interface LiffError {
    code: LiffErrorCode;
    message: string;
}

interface LiffUserProfile {
    userId: string;
    displayName: string;
    pictureUrl: string;
    statusMessage: string;
}

interface LINEBluetoothRequestDeviceFilter {
  deviceId: string;
}

interface RequestDeviceOptions {
  filters: LINEBluetoothRequestDeviceFilter[];
}

type BluetoothDeviceEvent = 'advertisementreceived' | 'gattserverdisconnected';

interface BluetoothDevice {
  id: string;
  name?: string;
  gatt?: BluetoothRemoteGATTServer;
  watchingAdvertisements: boolean;
  watchAdvertisements(): Promise<void>;
  unwatchAdvertisements(): void;
  addEventListener(type: BluetoothDeviceEvent, listener?: EventListenerOrEventListenerObject, options?: boolean | AddEventListenerOptions): void;
  removeEventListener(type: BluetoothDeviceEvent, callback?: EventListenerOrEventListenerObject, options?: EventListenerOptions | boolean): void;
}

type BluetoothRemoteGATTCharacteristicEvent = 'characteristicvaluechanged';

interface BluetoothRemoteGATTCharacteristic {
  service?: BluetoothRemoteGATTService;
  uuid: string;
  value?: DataView;
  readValue(): Promise<DataView>;
  writeValue(value: BufferSource): Promise<void>;
  startNotifications(): Promise<BluetoothRemoteGATTCharacteristic>;
  stopNotifications(): Promise<BluetoothRemoteGATTCharacteristic>;
  addEventListener(type: BluetoothRemoteGATTCharacteristicEvent, listener?: EventListenerOrEventListenerObject, options?: boolean | AddEventListenerOptions): void;
  removeEventListener(type: BluetoothRemoteGATTCharacteristicEvent, callback?: EventListenerOrEventListenerObject, options?: EventListenerOptions | boolean): void;
}

interface BluetoothRemoteGATTService {
  device: BluetoothDevice;
  uuid: string;
  getCharacteristic(characteristicUUID: string): Promise<BluetoothRemoteGATTCharacteristic>;
}

interface BluetoothRemoteGATTServer {
  device: BluetoothDevice;
  connected: boolean;
  connect(): Promise<BluetoothRemoteGATTService>;
  disconnect(): void;
  getPrimaryService(serviceUUID: string): Promise<BluetoothRemoteGATTService>;
}

export interface LiffBluetooth {
  referringDevice?: BluetoothDevice;
  getAvailability(): Promise<boolean>;
  requestDevice(options?: RequestDeviceOptions): Promise<BluetoothDevice>;
}

export interface LIFF {
    bluetooth: LiffBluetooth;
    init(config: { liffId: string }): Promise<void>;
    init(config: { liffId: string }, successCallback: () => void, errorCallback: (error: LiffError) => void): void;
    getOS(): 'ios' | 'android' | 'web';
    getLanguage(): string;
    getVersion(): string;
    isInClient(): boolean;
    isLoggedIn(): boolean;
    login(loginConfig?: { redirectUri?: string }): void;
    logout(): void;
    getAccessToken(): string;
    getProfile(): Promise<LiffUserProfile>;
    // Array of Message objects
    // Max: 5
    // You can send the following types of Messaging API messages:
    // Text message
    // Image message
    // Video message
    // Audio message
    // Location message
    // Template message. However, only a URI action can be set as an action.
    // Flex Message
    sendMessages(messages: Message[]): Promise<void>;
    openWindow(params: {
        url: string
        external: boolean
    }): void;
    scanCode(): Promise<{ value: string }>;
    closeWindow(): void;
    initPlugins(pluginList: string[]): Promise<void>;
}
