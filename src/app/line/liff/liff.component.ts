import { environment } from './../../../environments/environment';
import { Component, OnInit } from '@angular/core';

import { NgxSpinnerService } from 'ngx-spinner';

import {ScriptService} from 'ngx-script-loader';
import { liff } from 'src/app/@types/liff-sdk';
// declare var liff: any;

@Component({
  selector: 'app-liff',
  templateUrl: './liff.component.html',
  styleUrls: ['./liff.component.scss']
})
export class LiffComponent implements OnInit {

  constructor(
    private scriptService: ScriptService,
    private spinner: NgxSpinnerService,
  ) {
    this.scriptService.loadScript('https://static.line-scdn.net/liff/edge/2.1/sdk.js').subscribe(() => {
      liff.init({liffId: environment.liffId}).then(() => {
        this.spinner.hide();
        const accessToken = liff.getAccessToken();
        liff.closeWindow();
      });
    });
  }

  ngOnInit() {
    this.spinner.show();
  }

}
