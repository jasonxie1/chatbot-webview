import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxSpinnerModule } from 'ngx-spinner';

import { LineRoutingModule } from './line-routing.module';
import { LiffComponent } from './liff/liff.component';


@NgModule({
  declarations: [LiffComponent],
  imports: [
    CommonModule,
    LineRoutingModule,
    NgxSpinnerModule,
  ]
})
export class LineModule { }
